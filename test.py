import torch 
import numpy as np 
import torch.nn as nn 
import json 

if __name__ == "__main__":
    json_string = ""
    json_data = []
    for l in open("./state_dict/dev_pred_bert_seq2seq.json", encoding="utf-8") :
        if "}" in l :
            json_string += l 
            json_data.append(eval(json_string))
            json_string = ""
        else :
            json_string += l
    
    print(json_data[:10])

    json_string2 = ""
    json_data2 = []
    for l in open("./state_dict/dev_pred_bert4keras.json", encoding="utf-8") :
        if "}" in l :
            json_string2 += l 
            json_data2.append(eval(json_string2))
            json_string2 = ""
        else :
            json_string2 += l
    
    print(json_data2[:10])

    for json1, json2 in zip(json_data, json_data2):
        if json1["lack"] != json2["lack"]:
            print(json1["lack"])
            print(json2["lack"])
            print("~~~~~~~~~~")